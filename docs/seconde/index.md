# Rappels de seconde sur Python et les algorithmes
## Affectation et variables
Dans un algorithme ou un programme en Python :

+ on utilise des **variables** ; 

+ les variables représentent des nombres ou d'autres objets (listes, chaînes de caractères, ...) ; 

+ on utilise une lettre ou un mot pour les désigner ; 

+ on modifie leur valeur lors d'**affectations**.


On effectue par exemple les affectations suivantes :

!!! note "Algorithme"
    $b \leftarrow 3$  
    $a\leftarrow 4$  
    $b\leftarrow a+b$

La traduction en Python est la suivante :

!!! note "En python"
    ```python
    b = 3
    a = 4
    b = a + b
    ```