# Synthèse du Cours de Terminale NSI

Ce site regroupe l'ensemble des cours de Terminale spécialité NSI.
Il est organisé en 4 parties, conformément au référentiel de la spécialité.

- Architectures matérielles, systèmes d’exploitation et réseaux
- Langages et algorithmique
- Structures de données
- Bases de données

La page de présentation de chaque partie reprend les attendus du référentiel de la spécialité
 pour la partie concernée. La présence d'une croix dans la première colonne indique que l'élève 
 n'est pas tenu d'avoir vu ce point pour passer l'épreuve de spécialité.
 
Les activités (TP) évoluant trop rapidement, elles ne sont pas présentes sur ce site mais 
uniquement sur un serveur [Jupyter](https://serveur-pgdg.net) d'accès reservé aux élèves du lycée Pierre Gilles de Gennes.