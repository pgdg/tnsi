# Paradigme de Programmation [^1]

Les langages de programmation permettent de traiter des problèmes selon différentes approches.

## Programmation impérative [^2]
**La programmation impérative** est un paradigme de programmation qui décrit les opérations en séquences d'instructions  
exécutées par l'ordinateur pour modifier l'état du programme.   
Ce type de programmation est le plus répandu parmi l'ensemble des langages de programmation existants.     
C, Pascal ou encore les interpréteurs de commandes Unix sont des langages impératifs.

## Les langages déclaratifs
Les **langages déclaratifs** permettent d'écrire la spécification du problème et laissent l'implémentation du langage trouver une façon 
efficace de réaliser les calculs nécessaires à sa résolution.   
SQL est un langage déclaratif que vous êtes susceptible de connaître.  
Une requête SQL décrit le jeu de données que vous souhaitez récupérer et le moteur SQL choisit la méthode de résolution 
(parcourir les tables ou utiliser les index, l'ordre de résolution des sous-clauses, etc).

## La programmation orientée objet
**Les programmes orientés objet** manipulent des ensembles d'objets. Ceux-ci possèdent un état interne et des méthodes qui interrogent ou 
modifient cet état d'une façon ou d'une autre.  
Smalltalk et Java sont deux langages orientés objet.  
C++ et Python gèrent la programmation orientée objet mais n'imposent pas l'utilisation de telles fonctionnalités.

## La programmation fonctionnelle
**La programmation fonctionnelle** implique de décomposer un problème en un ensemble de fonctions.  
Dans l'idéal, les fonctions produisent des sorties à partir d'entrées et ne possède pas d'état interne qui soit susceptible de modifier 
la sortie pour une entrée donnée (**pas d'effets de bord**).  
Les langages fonctionnels les plus connus sont ceux de la famille ML (Standard ML, OCaml et autres) et Haskell.  

Il existe des avantages théoriques et pratiques au style fonctionnel :  

- preuves formelles : Construire une preuve mathématique de l'exactitude d'un programme fonctionnel,
- modularité : elle impose de décomposer le programme en petits morceaux,
- composabilité : réutilisation des fonctions déjà écrites.
- facilité de débogage et de test : Tester et déboguer un programme fonctionnelle est plus facile.

[^1]: Source : [docs.python : Guide pratique de la programmation fonctionnelle](https://docs.python.org/fr/3/howto/functional.html)
[^2]: Source : [Wikipedia : Programmation impérative](https://fr.wikipedia.org/wiki/Programmation_imp%C3%A9rative)
