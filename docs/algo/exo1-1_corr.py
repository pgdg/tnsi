### Définition de la fonction Somme()
def somme(liste : list):
    """
    Cette fonction calcule récursivement la somme des éléments de la liste list
    """
    S = 0
    if len(liste) == 1:
        return liste[0]
    else:
        return liste[0] + somme(liste[1:])

