site_name: Cours de Terminale NSI

theme:
  name: material
  #custom_dir: overrides
  custom_dir: my_theme_customizations/
  features:
        - navigation.instant
        - navigation.tabs
        - navigation.expand
        - navigation.top
        - toc.integrate
        - header.autohide

  language: fr
  font: false                     # RGPD ; pas de fonte Google
  logo: assets/logo.png
  palette:                        # Palettes de couleurs jour/nuit
      - media: "(prefers-color-scheme: light)"
        scheme: default
        primary: indigo
        accent: indigo
        toggle:
            icon: material/weather-sunny
            name: Passer au mode nuit
      - media: "(prefers-color-scheme: dark)"
        scheme: slate
        primary: blue
        accent: blue
        toggle:
            icon: material/weather-night
            name: Passer au mode jour
#  icon:
 #   admonition:
  #    note: fontawesome/solid/sticky-note
   #   abstract: fontawesome/solid/book
    #  info: fontawesome/solid/info-circle
#      tip: fontawesome/solid/bullhorn
#      success: fontawesome/solid/check
#      question: fontawesome/solid/question-circle
#      warning: fontawesome/solid/exclamation-triangle
#      failure: fontawesome/solid/bomb
 #     danger: fontawesome/solid/skull
 #     bug: fontawesome/solid/robot
  #    example: fontawesome/solid/flask
 #     quote: fontawesome/solid/quote-left
 #     def: fontawesome/solid/book


nav:
  - Accueil: index.md
  - Architecture et OS: 
    - Présentation: archi_et_os/contenu.md
    - System on Chip: archi_et_os/soc.md
    - Gestion de processus: archi_et_os/processus.md
    - Protocole de routage: archi_et_os/routage.md
    - Transmission sécurisée: archi_et_os/transmission.md
  - Langages et algorithmique: 
    - Présentation: algo/contenu.md
    - 1. Récursivité: algo/recursif.md
    - 2. Diviser pour régner: algo/diviser.md
    - 3. Programmation dynamique: algo/dynamique.md
    - 4. Calculabilité: algo/calculabilite.md
    - 5. Paradigme de programmation : algo/paradigme.md
    - 6. Programmation fonctionnelle : algo/fonctionnelle.md
    - 7. Recherche textuelle: algo/textuelle.md
  - Structures de données:
    - Présentation: s_donnees/contenu.md
    - 1. Rappels: s_donnees/rappels.md
    - 2. Programmation Orientée Objet: s_donnees/poo.md
    - 3. Listes-Piles-Files: s_donnees/li_pi_fi.md
    - 4. Arbres: s_donnees/arbres.md
    - 5. Arbres Biniaires de Recherche: s_donnees/abr.md
    - 6. Graphes: s_donnees/graphes.md
  - Bases de données:
    - Présentation: bdd/contenu.md
    - Les bases de données: bdd/databases.md

markdown_extensions:
    - meta
    - abbr

    - def_list                      # Les listes de définition.
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - admonition                    # Blocs colorés  !!! info "ma remarque"
    - pymdownx.details              #   qui peuvent se plier/déplier.
    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.highlight            # Coloration syntaxique du code
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
        custom_checkbox:    false   #   avec cases d'origine
        clickable_checkbox: true    #   et cliquables.
    - pymdownx.tabbed:               # Volets glissants.  === "Mon volet"
        alternate_style: true 
    - pymdownx.superfences:          # Imbrication de blocs.
        preserve_tabs: true
    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.emoji:               # Émojis  :boom:
        emoji_index:     !!python/name:materialx.emoji.twemoji
        emoji_generator: !!python/name:materialx.emoji.to_svg
    - pymdownx.tasklist:
        custom_checkbox: true
        clickable_checkbox: true
    - pymdownx.arithmatex:
        generic: true
    - toc:
        permalink: ⚓︎
        toc_depth: 3


#    - pymdownx.details              #   qui peuvent se plier/déplier.
#    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
#    - pymdownx.mark                 # Passage ==surligné==.
#    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
#    - pymdownx.highlight            # Coloration syntaxique du code
#    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
#    - pymdownx.snippets             # Inclusion de fichiers externe.
#    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
#        custom_checkbox:    false   #   avec cases d'origine
#        clickable_checkbox: true    #   et cliquables.
#    - pymdownx.tabbed               # Volets glissants.  === "Mon volet"
#    - pymdownx.superfences          # Imbrication de blocs.
#    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
#        separator: "\uff0b"
#    - pymdownx.emoji:
#        emoji_index: !!python/name:materialx.emoji.twemoji
#        emoji_generator: !!python/name:materialx.emoji.to_svg
#        options:
#          custom_icons:
#            - overrides/.icons


#    - pymdownx.arithmatex:
#        generic: true
#    - toc:
#        permalink: ⚓︎
#        toc_depth: 3


extra:
  social:
    - icon: fontawesome/brands/twitter
      link: https://serveur-pgdg.net:8080/
      name: serveur Jupyter
copyright: Copyright &copy; 2019 - 2021 Éric ROUGIER / Paul GODARD

plugins:
  - search
  - macros

extra_css:
  - xtra/stylesheets/pyoditeur.css
  - xtra/stylesheets/ajustements.css   

extra_javascript:
  - xtra/javascripts/mathjax-config.js                    # MathJax
  - javascripts/config.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
  - xtra/javascripts/interpreter.js

  #- javascripts/config.js
  #- https://polyfill.io/v3/polyfill.min.js?features=es6
  #- https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
  #- https://unpkg.com/mermaid@8.9.2/dist/mermaid.min.js
  #- javascripts/mermaid.js
  #- xtra/javascripts/interpreter.js
